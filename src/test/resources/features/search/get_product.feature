Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Background:
    Given endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/"

  Scenario: Search for a valid product "orange"
    When the user calls endpoint with the product "orange"
    Then the response collection has field "title" contain value "orange"
    And the response status code is 200

  Scenario: Search for a valid product "apple"
    When the user calls endpoint with the product "apple"
    Then the response collection has field "title" contain value "apple"
    And the response status code is 200

  Scenario: Search for a valid product "pasta"
    When the user calls endpoint with the product "pasta"
    Then the response collection has field "title" contain value "pasta"
    And the response status code is 200

  Scenario: Search for a valid product "cola"
    When the user calls endpoint with the product "cola"
    Then the response collection has field "title" contain value "cola"
    And the response status code is 200

  Scenario: Search for a valid product "orange" and check that "isPromo" fields contain value "false"
    When the user calls endpoint with the product "orange"
    Then the response collection has field "isPromo" contain value "false"
    And the response status code is 200


  Scenario: Search for a product with incorrect name "OrAnGe"
    When the user calls endpoint with the product "OrAnGe"
    Then the response is object with "detail.message" contain value "Not found"
    And the response status code is 404

  Scenario: Search for a product with incorrect name "cars"
    When the user calls endpoint with the product "cars"
    Then the response is object with "detail.message" contain value "Not found"
    And the response status code is 404


  Scenario: Search for a valid product with invalid HTTP method "POST"
    When the user sends a "POST" request to the product "orange"
    Then the response status code is 405

  Scenario: Search for a valid product with invalid HTTP method "delete"
    When the user sends a "delete" request to the product "orange"
    Then the response status code is 405
    

  Scenario: Search for a valid product with incorrect URL ../api/v1/search/demo2/
    Given endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo2/"
    When the user calls endpoint with the product "cola"
    Then the response status code is 404

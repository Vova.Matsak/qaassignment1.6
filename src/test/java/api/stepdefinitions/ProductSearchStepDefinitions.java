package api.stepdefinitions;

import api.helpers.HttpCallManager;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ProductSearchStepDefinitions {
    private static String endpoint;
    private static Response response;
    private final HttpCallManager httpCallManager = new HttpCallManager();

    private void callProduct(String product) {
        response = httpCallManager.getRequest(endpoint + product);
    }

    @Given("endpoint {string}")
    public void getEndpoint(String endpoint) {
        ProductSearchStepDefinitions.endpoint = endpoint;
    }

    @When("the user calls endpoint with the product {string}")
    public void theUserCallsEndpointWithProduct(String product) {
        callProduct(product);
    }

    @Then("the response collection has field {string} contain value {string}")
    public void theResponseCollectionHasFieldWithValue(String fieldName, String product) {
        //  Assert that all <fieldName> fields contain the string <value> (ignoring case) in collection
        List<Object> fieldNames = response.jsonPath().getList(fieldName);
        String message = "Not all field '" + fieldName + "' contain the value '" + product + "'.\n" + fieldName + "s: " + fieldNames;
        assertThat(message, fieldNames.stream().allMatch(field -> field.toString().toLowerCase().contains(product.toLowerCase())));
    }

    @And("the response status code is {int}")
    public void theResponseStatusCodeShouldBe(int statusCode) {
        response.then().statusCode(statusCode);
    }

    @Then("the response is object with {string} contain value {string}")
    public void theResponseIsObjectWithContainValue(String fieldName, String value) {
        response.then().body(fieldName, is(value));
    }

    @When("the user sends a {string} request to the product {string}")
    public void theUserSendsARequestToTheProduct(String method, String product) {
        //  Assert that response object <fieldName> field contains the string <value>
        response = httpCallManager.stringMethodSelector(method.toUpperCase(), endpoint + product);
    }
}

package api.helpers;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class HttpCallManager {
    public Response getRequest(String path) {
        return given()
                .contentType(ContentType.JSON)
                .when()
//                .log().all()
                .get(path)
                .then()
//                .log().all()
                .extract().response();
    }

    public Response postRequest(String path, Object body) {
        return given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
//                .log().all()
                .post(path)
                .then()
//                .log().all()
                .extract().response();
    }

    public Response putRequest(String path, Object body) {
        return given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
//                .log().all()
                .put(path)
                .then()
//                .log().all()
                .extract().response();
    }

    public Response deleteRequest(String path) {
        return given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
//                .log().all()
                .delete(path)
                .then()
//                .log().all()
                .extract().response();
    }

    public Response stringMethodSelector(String method, String path) {
        switch(method) {
            case "POST":
                return postRequest(path, "");
            case "PUT":
                return putRequest(path, "");
            case "DELETE":
                return deleteRequest(path);
            case "GET":
                return getRequest(path);
        }
        return null;
    }
}

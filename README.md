# Installation

1. Install Java Development Kit (JDK) version 8 and set up the Java Environment Variables.
  * Download and install JDK 8 (e.g. from https://adoptium.net/temurin/releases/?version=8)
  * Add **JAVA_HOME** system environment variable with path to your installed jdk - _JDK8path_
  * Add _JDK8path/bin_ in your **Path** variable
2. Install Apache Maven / Gradle and add correponding _path/bin_ into **Path** environment variables.
3. Clone the project repository from GitLab. (https://gitlab.com/Vova.Matsak/qaassignment1.6.git)
4. Open the project in your preferred IDE.
5. Install the necessary dependencies by running `gradle build` or `mvn install`.

# Running the Tests

* Navigate to the project root directory.
* Run the following command to execute the tests: `mvn test` or `gradle test`
* After test run you can open the results in **\target\site\serenity\index.html**
  
# Writing New Tests

* Create a new feature file under **src/test/resources/features** directory with the **.feature** extension.
* You can also modify existing **.feature** files to add or adjust tests
* Write your test scenarios in Gherkin syntax.
* Create a new step definition file under **src/test/java/api/stepdefinitions** directory or use existing. 
If you add new **.feature** file make sure that you create corresponding Test Suite similar to **src/test/java/api/ProductSearchTestSuite.java** 
* Write your step definitions in Java code, mapping them to the corresponding Gherkin steps.
* Run the tests using the steps described in the "Running the Tests" section to verify that your new test scenarios are executed successfully.

***
# Refactoring changes:

- Updated the build configuration for Gradle or Maven and removed unnecessary dependencies related to UI testing since only API tests are performed in this project.
- Cleaned up the project by removing unused files and parts related to UI testing, such as the `serenity.conf` file.
- Renamed some files and adjusted the project structure to improve its organization and readability.
- Added some scenarios for feature testing as templates to further improve test coverage and ensure better test quality.
- Implemented a class for making the main REST calls, which can be used in the step definitions for the API tests.
- Added a GitLab CI/CD configuration for automating the build and test process, with automatic build and manual test jobs. The manual `Test` job generates artifacts containing an HTML report of the test run.
